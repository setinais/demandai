<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="robots" content="all,follow">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="{{ asset('css/style.default.css') }}" rel="stylesheet">

</head>
<body>
    <div id="app">
        <nav class="side-navbar">
            <div class="side-navbar-wrapper">
                <!-- Sidebar Header    -->
                <div class="sidenav-header d-flex align-items-center justify-content-center">
                    <!-- User Info-->
                    <div class="sidenav-header-inner text-center"><img src="{{ asset('vendor/img/logo.png')}}" style="width: 100px;" alt="IFTO" >
                        <h2 class="h5">{{ Auth::user()->name }}</h2><span>{{ Auth::user()->role }}</span>
                    </div>
                    <!-- Small Brand information, appears on minimized sidebar-->
                    <div class="sidenav-header-logo"><a href="{{ route('home') }}" class="brand-small text-center"> <img src="{{ asset('vendor/img/logo.png')}}" style="width: 100px;" alt="IFTO" ></a></div>
                </div>
                <!-- Sidebar Navigation Menus-->

                <div class="main-menu">
                    <h5 class="sidenav-heading">Menu</h5>
                    <ul id="side-main-menu" class="side-menu list-unstyled">
                        <li><a href="{{ route('home') }}"> <i class="fa fa-home"></i>Home</a></li>
                        <li><a href="{{ route('demandai') }}"> <i class="fa fa-paper-plane"></i>Demandai</a></li>
                        @foreach (Auth::user()->menu() as $menu)
                        <li><a href="{{ route($menu['module']) }}"> <i class="fa {{ "fa-".$menu['icon'] }}"></i>{{$menu['description']}}</a></li>
                        @endforeach
                    </ul>
                </div>

            </div>
        </nav>
        <div class="page">
            <!-- navbar-->
            <header class="header">
                <nav class="navbar">
                    <div class="container-fluid">
                        <div class="navbar-holder d-flex align-items-center justify-content-between">
                            <example-component></example-component>
                            <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                                <!-- Notifications dropdown-->
                                <li class="nav-item dropdown"> <a id="notifications" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link"><i class="fa fa-bell"></i><span class="badge badge-warning">12</span></a>
                                    <ul aria-labelledby="notifications" class="dropdown-menu">
                                        <li><a rel="nofollow" href="#" class="dropdown-item">
                                                <div class="notification d-flex justify-content-between">
                                                    <div class="notification-content"><i class="fa fa-envelope"></i>You have 6 new messages </div>
                                                    <div class="notification-time"><small>4 minutes ago</small></div>
                                                </div></a>
                                        </li>
                                    </ul>
                                </li>
                                <!-- Messages dropdown-->

                                <!-- Languages dropdown    -->
                                <li class="nav-item dropdown">
                                    <a id="languages" rel="nofollow" href="{{ route('perfil',['status' => '0']) }}" aria-haspopup="true"  class="nav-link language ">
                                        <span class="d-none d-sm-inline-block">Perfil</span>
                                        <i class="fa fa-user-circle"></i>
                                    </a>
                                </li>
                                <!-- Log out-->
                                <li class="nav-item"><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();" class="nav-link logout"> <span class="d-none d-sm-inline-block">Sair</span><i class="fa fa-sign-out-alt"></i></a></li>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </ul>
                        </div>
                    </div>
                </nav>
            </header>
        <main class="py-1">
            @if(session('deletarErro'))
                <div class="alert alert-danger"><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                    <strong>Atenção!</strong>
                    {{ session('deletarErro') }}
                </div>
            @endif
            @yield('content')
        </main>
            <footer class="main-footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            <p>Demandai &copy; 2019</p>
                        </div>
                        <div class="col-sm-6 text-right ">
                            <p>SI <a href="https://bootstrapious.com/p/bootstrap-4-dashboard" class="external text-light">7º Periodo 2019/1</a></p>

                        </div>
                    </div>
                </div>
            </footer>
        </div>
        <!-- JavaScript files-->
    </div>
</body>
</html>

