<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="">
        <meta name="robots" content="all,follow">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('vendor/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <link href="{{ asset('css/style.default.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/welcome.css') }}"/>
    </head>
    <body>
        <nav class="navbar navbar-light bg-success">
            <div class="container">
                <a class="navbar-brand" href="#">
                    <img src="{{ asset('vendor/img/logo.png')}}" width="170" height="100" alt=""/>
                </a>
                <div class="logo text-uppercase text-center"><strong class="text-light text-bold"><h1>Demandaí</h1></strong></div>
                <div class="1text-right ">
                    <button type="button" data-toggle="modal" data-target="#btn-consultar" class="btn btn-success">Consultar demanda</button>


                    @guest
                        <button type="button" data-toggle="modal" data-target="#btn-entrar" class="btn btn-success">Entrar</button>
                    @else
                        <a id="navbarDropdown" class="btn btn-success" href="{{ route('home') }}" role="button">
                            {{ Auth::user()->name }}
                        </a>
                    @endguest
                </div>
            </div>
        </nav>
        <div id="app">
            @yield('content')
        </div>
        <footer class="bg-success">
            <div class="copyrights">
                <p><a href="http://portal.ifto.edu.br/paraiso/" class="external">Quem somos</a></p>
            </div>
            <div class="copyrights text-center">
                <p>Desenvolvido por Residência 1.0 <a href="http://portal.ifto.edu.br/paraiso/" class="external">Todos os direitos reservados</a></p>
            </div>
        </footer>
    </body>
</html>