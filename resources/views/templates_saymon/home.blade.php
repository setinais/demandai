@extends('layouts.home')
@section('content')
        <div class="page login-page">
          <div class="container">


           <div class="col-lg-12">
            <div class="card-body text-right">
              <button type="button" data-toggle="modal" data-target="#btn-consultar" class="btn btn-primary">Consultar demanda

                <!-- ICON DA LUPA, AINDA VOU ARRUMAR

                  <i class="icon-search"></i>

                --></button>

              <button type="button" data-toggle="modal" data-target="#btn-entrar" class="btn btn-primary">Entrar</button>

              <!--MODAL DE CONSULTAR DEMANDA-->
              <div id="btn-consultar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                <div role="document" class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 id="exampleModalLabel" class="modal-title">Consultar demanda</h5>
                      <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                      <p>Informe seus dados para realizar a busca.</p>
                      <form>
                        <div class="form-group">
                          <label>Código de acesso</label>
                          <div class="form-group row has-success">
                            <div class="col-sm-12">
                              <input type="text" class="form-control is-valid">
                            </div>
                          </div>                          
                        </div>

                        <div class="form-group">
                          <label>CPF</label>
                          <div class="form-group row has-success">
                            <div class="col-sm-12">
                              <input type="text" class="form-control is-valid">
                            </div>
                          </div>                          
                        </div>


                        <div class="form-group">       
                          <input type="submit" value="Consultar" class="btn btn-primary">
                        </div>
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="button" data-dismiss="modal" class="btn btn-secondary">Voltar</button>
                    </div>
                  </div>
                </div>
              </div>

              <!--MODAL DE ENTRAR-->
              <div id="btn-entrar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                <div role="document" class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 id="exampleModalLabel" class="modal-title">Entrar</h5>
                      <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                      <p>Informe seus dados para acessar o sistema.</p>
                      <form>
                          <div class="form-group">
                          <label>Login</label>
                          <div class="form-group row has-success">
                            <div class="col-sm-12">
                              <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text">@</span></div>
                                <input type="text" placeholder="Login" class="form-control">
                              </div>
                            </div>
                          </div>                          
                        </div>

                        <div class="form-group">
                          <label>Senha</label>
                          <div class="form-group row has-success">
                            <div class="col-sm-12">
                              <div class="input-group">
                                <div class="input-group-prepend"><span class="input-group-text">*</span></div>
                                <input type="text" placeholder="********" class="form-control">
                              </div>
                            </div>
                          </div>                          
                        </div>

                        <div class="form-group">       
                          <input type="submit" value="Consultar" class="btn btn-primary">
                        </div>
                      </form>
                    </div>
                    <div class="modal-footer">
                      <button type="button" data-dismiss="modal" class="btn btn-secondary">Voltar</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
          <!-- FIM DA MODAL DE ENTRAR-->

          <div class="home-do-saymon">
            <div class="form-inner text-center">
              <div class="logo text-uppercase"><strong class="text-primary">Demandaí</strong></div>
              <div class="card-header">
                <h4>Sistema de prospecção entre demadantes e servidores IFTO.</h4>
              </div>

              <div class="col-sm-10" align="center">
                <div class="form-group">
                  <div class="input-group">
                    <input type="text" placeholder="Digite sua demanda" class="form-control">
                    <div class="input-group-append">
                      <button type="button" class="btn btn-primary"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"><i class="fa fa-search"></i></font></font></button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- TABELA COM RESULTADO DA PESQUISA -->
              
              <div class="col-lg-10 container">
                <div class="card">                    
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="table table-striped table-hover">
                        <thead>
                          <tr align="text-center">
                            <th>Serviços</th>
                            <th>Laboratórios</th>
                            <th>Equipamentos</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Análise de dados</td>
                            <td>Laboratório de Residência</td>
                            <td>Servidor de banco de dados</td>
                          </tr>
                          <tr>
                            <td>Desenvolvimento de aplicativos mobile</td>
                            <td>Laboratório de Química</td>
                            <td>Robôs de lego</td>
                          </tr>
                          <tr>
                            <td>Desenvolvimento de web sites</td>
                            <td>Laboratório de Redes</td>
                            <td>Projetos de imagem</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>          

              <div class="form-group">       
                <input type="submit" value="Demandaí" class="btn btn-primary">
              </div>
            </div>              
            <div class="copyrights">
              <p><a href="http://portal.ifto.edu.br/paraiso/" class="external">Quem somos</a></p>
            </div>
            <div class="copyrights text-center">
              <p>Desenvolvido por Residência 1.0 <a href="http://portal.ifto.edu.br/paraiso/" class="external">Todos os direitos reservados</a></p>
            </div>
          </div>
        </div>
      </div>

      <!--CARVALHO, COLOCA ISSO NO CSS, NOVA TAG PARA NAO ALTERAR A DA PAGINA DE LOGIN-->
      <style type="text/css">
        .home-do-saymon {
          min-height: 100vh;
          max-width: 100%;
          margin: 0 auto;
          padding: 20px 0;
          position: relative;
        }
      </style>

@endsection