@extends('layouts.home')
@section('content')
<div class="login-page ">
  <div class="container">

    <div class="col-lg-12 ">

      <div class="card-body text-right">

        <!--INÍCIO DA MODAL DE SELECIONAR TIPO DE DEMANDA-->    
        <div id="btn-tipodemanda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
          <div role="document" class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <h5 id="exampleModalLabel" class="modal-title">DEMANDAÍ</h5>
                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
              </div>
              <div class="modal-body">
                <p>Selecione o tipo de demanda.</p>

                <section class="dashboard-counts section-padding">
                  <div class="container-fluid">                    
                    <div class="row">

                      <div class="col-xl col-md-4 col-6">
                        <div class="wrapper count-title d-flex">
                          <div class="icon"><i class="fa fa-cogs"></i></div>
                          <div class="name"><strong class="text-uppercase">Serviços</strong><span>Disponíveis</span>
                            <div class="count-number">25</div> <!-- PUXAR DO BANCO O NÚMERO DE SERVIÇOS-->
                          </div>
                        </div>
                      </div>

                      <div class="col-xl col-md-4 col-6">
                        <div class="wrapper count-title d-flex">
                          <div class="icon"><i class="fa fa-tag"></i></div>
                          <div class="name"><strong class="text-uppercase">Laboratórios</strong><span>Disponíveis</span>
                            <div class="count-number">400</div> <!-- PUXAR DO BANCO O NÚMERO DE LABORATÓRIOS-->
                          </div>
                        </div>
                      </div>

                      <div class="col-xl col-md-4 col-6">
                        <div class="wrapper count-title d-flex">
                          <div class="icon"><i class="fa fa-desktop"></i></div>
                          <div class="name"><strong class="text-uppercase">Equipamentos</strong><span>Disponíveis</span>
                            <div class="count-number">342</div> <!-- PUXAR DO BANCO O NÚMERO DE EQUIPAMENTOS-->
                          </div>
                        </div>
                      </div> 
                    </div>
                  </div>
                </section>
                <button type="button" data-toggle="modal" data-target="#btn-demandai" class="btn btn-primary">Selecionar</button>
              </div>

              <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-secondary">Voltar</button>
              </div>
            </div>
          </div>
        </div>
        <!--FIM MODAL DE SELECIONAR TIPO DE DEMANDA-->

        <!--MODAL DE CONSULTAR DEMANDA-->
        <div id="btn-consultar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
          <div role="document" class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h5 id="exampleModalLabel" class="modal-title">Consultar demanda</h5>
                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
              </div>
              <div class="modal-body">
                <p>Informe seus dados para consultar suas demandas.</p>
                <form>

                  <div class="form-group" >
                    <label>Código de acesso</label>
                    <div class="form-group row has-success">
                      <div class="col-sm-12">
                        <input type="text" class="form-control is-valid">
                      </div>
                    </div>                          
                  </div>

                  <div class="form-group">
                    <label>CPF</label>
                    <div class="form-group row has-success">
                      <div class="col-sm-12">
                        <input type="text" class="form-control is-valid">
                      </div>
                    </div>                          
                  </div>


                  <div class="form-group">       
                    <input type="submit" value="Consultar" class="btn btn-primary">
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-secondary">Voltar</button>
              </div>
            </div>
          </div>
        </div>
        <!--INÍCIO DA MODAL DE ENTRAR-->

        <div id="btn-entrar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
          <div role="document" class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h5 id="exampleModalLabel" class="modal-title">Entrar</h5>
                <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
              </div>
              <div class="modal-body">
                <p>Informe seus dados para acessar o sistema.</p>
                <form action="{{ route('login') }}" method="POST">
                  @csrf
                  <div class="form-group">
                    <label>Login</label>
                    <div class="form-group row has-success">
                      <div class="col-sm-12">
                        <div class="input-group">
                          <div class="input-group-prepend"><span class="input-group-text">@</span></div>
                          <input type="text" placeholder="Login" name="email" class="form-control">
                        </div>
                      </div>
                    </div>                          
                  </div>

                  <div class="form-group">
                    <label>Senha</label>
                    <div class="form-group row has-success">
                      <div class="col-sm-12">
                        <div class="input-group">
                          <div class="input-group-prepend"><span class="input-group-text">*</span></div>
                          <input type="password" name="password" placeholder="********" class="form-control">
                        </div>
                      </div>
                    </div>                          
                  </div>

                  <div class="form-group">       
                    <input type="submit" value="Entrar" class="btn btn-primary">
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-secondary">Voltar</button>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
    <!-- FIM DA MODAL DE ENTRAR-->

    <!--INÍCIO DA MODAL DE ENTRAR-->

    <div id="btn-entrar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
      <div role="document" class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 id="exampleModalLabel" class="modal-title">Entrar</h5>
            <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
          </div>
          <div class="modal-body">
            <p>Informe seus dados para acessar o sistema.</p>
            <form>
              <div class="form-group">
                <label>Login</label>
                <div class="form-group row has-success">
                  <div class="col-sm-12">
                    <div class="input-group">
                      <div class="input-group-prepend"><span class="input-group-text">@</span></div>
                      <input type="text" placeholder="Login" class="form-control">
                    </div>
                  </div>
                </div>                          
              </div>

              <div class="form-group">
                <label>Senha</label>
                <div class="form-group row has-success">
                  <div class="col-sm-12">
                    <div class="input-group">
                      <div class="input-group-prepend"><span class="input-group-text">*</span></div>
                      <input type="text" placeholder="********" class="form-control">
                    </div>
                  </div>
                </div>                          
              </div>

              <div class="form-group">       
                <input type="submit" value="Entrar" class="btn btn-primary">
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" data-dismiss="modal" class="btn btn-secondary">Voltar</button>
          </div>
        </div>
      </div>
    </div>

    <!-- FIM DA MODAL DE ENTRAR-->

    <div class="home">
      <div class="form-inner ">



        <!-- TABELA COM RESULTADO DA PESQUISA -->


        <home-component></home-component>


      </div>              

    </div>
  </div>
</div>


<!--CARVALHO, COLOCA ISSO NO CSS, NOVA TAG PARA NAO ALTERAR A DA PAGINA DE LOGIN-->

@endsection