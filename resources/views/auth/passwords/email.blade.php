@extends('layouts.auth')

@section('content')
    <div class="container">
        <div class="d-flex justify-content-center h-100">

                <div class="card">
                    <div class="card-header">
                        <h3>{{ __('Resetar Senha') }}</h3>
                        <div class="d-flex justify-content-end social_icon">
                            <div class="login-logo"><a href="{{ url(config('adminlte.dashboard_url', 'home')) }}"><img src="{{ asset('vendor/img/logo.png')}}" alt="" height="130px"></a></div>
                        </div>
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form method="POST" action="{{ route('password.email') }}">
                            @csrf
                            <div class="form-group input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                                </div>
                                <input placeholder="E-mail" id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-10 offset-md-2">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Enviar email de recuperação!') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

        </div>
    </div>
@endsection
