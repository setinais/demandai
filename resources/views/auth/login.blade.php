@extends('layouts.auth')

@section('content')
<div class="container">
    <div class="d-flex justify-content-center h-100">
        <div class="card">
            <div class="card-header">
                <h3>Login</h3>
                <div class="d-flex justify-content-end social_icon">
                    <div class="login-logo"><a href="{{ url(config('adminlte.dashboard_url', 'home')) }}"><img src="{{ asset('vendor/img/logo.png')}}" alt="" height="130px"></a></div>

                </div>
            </div>
            <div class="card-body">
                @if ($errors->has('email'))
                    <span class="help-block badge badge-pill badge-danger">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                @endif
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                        </div>
                        <input required type="email" name="email" class="form-control" placeholder="Email">

                    </div>

                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                        </div>
                        <input required type="password" name="password" id="senha" class="form-control" placeholder="Senha">
                    </div>
                    <div class="row align-items-center remember">
                        <input name="remember" type="checkbox">Lembrar-me
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Login" class="btn float-right login_btn">
                    </div>
                </form>
            </div>
            <div class="card-footer">
                <div class="d-flex justify-content-center links">
                    Não tem cadastro?<a href="#">Cadastre-se</a>
                </div>
                <div class="d-flex justify-content-center">
                    @if (Route::has('password.request'))
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            {{ __('Esqueceu a Senha?') }}
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

