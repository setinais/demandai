@extends('layouts.app')
@section('content')

    <div class="container">
        @if(isset($msg))
            <div class="alert alert-{{$msg['type']}}"><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                <strong>Atenção!</strong>
                {{ $msg['msg'] }}
            </div>
        @endif
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h2>Olá <strong>{{ Auth::user()->name }}</strong></h2>
            </div>
            <div class="card-body">
                <p>Aqui contém todos os dados sobre você!.</p>
                <form class="form-horizontal" method="post" action="{{ route('updateperfil') }}">
                    @csrf
                    <div class="form-group row">
                        <label class="col-sm-2">Email</label>
                        <div class="col-sm-10">
                            <input type="email" value="{{ Auth::user()->email }}" class="form-control" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2">Nome</label>
                        <div class="col-sm-10">
                            <input id="inputHorizontalWarning" type="text" name="name" value="{{ Auth::user()->name }}" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}">
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2">Formação</label>
                        <div class="col-sm-10">
                            <input id="inputHorizontalWarning" type="text" name="formation" value="{{ Auth::user()->formation }}" class="form-control {{ $errors->has('formation') ? ' is-invalid' : '' }}">
                            @if ($errors->has('formation'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('formation') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2">Link Lattes</label>
                        <div class="col-sm-10">
                            <input id="inputHorizontalWarning" type="text" name="lattes" value="{{ Auth::user()->lattes }}" class="form-control {{ $errors->has('lattes') ? ' is-invalid' : '' }}">
                            @if ($errors->has('lattes'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('lattes') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2">Cargo Empenhado</label>
                        <div class="col-sm-10">
                            <input id="inputHorizontalWarning" type="text" value="{{ Auth::user()->role }}" class="form-control" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2">Modulos Liberados</label>
                        <div class="col-sm-10">
                            @php
                            $text = '';
                                foreach(Auth::user()->accessControl as $controls){

                                        $text .= ' ( '.$controls->module->description.' ) ';

                                }
                            echo "<input id='inputHorizontalWarning' type='text' value=' $text ' class='form-control' disabled>";
                            @endphp
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2">Instituição</label>
                        <div class="col-sm-10 form-group row has-success">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <select name="institution" class="custom-select {{ $errors->has('institution') ? ' is-invalid' : '' }}" required >
                                        @foreach( $instituicoes as $value )
                                            @if(Auth::user()->institution_id == $value->id)
                                                <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
                                            @endif
                                            @if(Auth::user()->institution_id != $value->id)
                                                <option value="{{ $value->id }}">{{ $value->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    @if ($errors->has('institution'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('institution') }}</strong>
                                        </span>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2">Senha</label>
                        <div class="col-sm-10">
                            <input type="password" placeholder="Digite a nova senha" name="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}">
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2">Confirmar Senha</label>
                        <div class="col-sm-10">
                            <input type="password" placeholder="Confirme a nova senha" name="password_confirmation" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}">
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-10 offset-sm-2">
                            <input type="submit" value="Atualizar" class="btn btn-primary">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
