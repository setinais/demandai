@extends('layouts.home')
@section('content')
    <div class="col-lg-6 col-md-6 container">
        <div class="card">
            <div class="card-header d-flex align-items-center">
                <h3>Faça sua Demanda
                    @isset($action)
                        @switch($action)
                            @case('lab')
                               {{' no laboratório '}} <span class="text-danger font-weight-bolder" id="name_pagina"> {{$nome}} </span>
                            @break

                            @case('ser')
                                {{' no serviço '}} <span class="text-danger font-weight-bolder" id="name_pagina"> {{$nome}} </span>
                            @break

                            @case('equ')
                                {{' no equipamento '}} <span class="text-danger font-weight-bolder" id="name_pagina"> {{$nome}} </span>
                            @break

                            @default
                            Default case...
                        @endswitch
                    @endisset
                </h3>
                @if(session('demanda'))
                    <div class="alert alert-success"><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                        <strong>Parabéns!</strong>
                        {{ session('demanda') }}
                    </div>
                @endif
            </div>
            <div class="card-body">
                    <form action="{{ route('demandai.store') }}" method="POST">
                        @isset($action)
                            <input type="hidden" id="action" value="{{ $action }}">
                            <input type="hidden" id="action_id" value="{{ $id_action }}">
                            <input type="hidden" id="nome" value="{{ $nome }}">
                        @endisset
                        @csrf
                        <div class="row">
                            <div class="form-group col-md-6 col-lg-6">
                                <label>Nome</label>
                                <div class="form-group row has-success">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text">@</span></div>
                                            <input type="text" name="nome" placeholder="Fabiano Assunção Santana" class="form-control {{ $errors->has('nome') ? ' is-invalid' : '' }}" value="{{ old('nome') }}">
                                            @if ($errors->has('nome'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('nome') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-5 col-lg-5">
                                <label>CPF</label>
                                <div class="form-group row has-success">
                                    <div class="col-lg-12 col-md-12">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text">@</span></div>
                                            <input type="text" name="cpf" placeholder="999.999.999-99" class="form-control {{ $errors->has('cpf') ? ' is-invalid' : '' }}" value="{{ old('cpf') }}">
                                            @if ($errors->has('cpf'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('cpf') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-4 col-lg-4">
                                <label>Data de nascimento</label>
                                <div class="form-group row has-success">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text">@</span></div>
                                            <input type="date" name="data_nascimento" placeholder="06/12/2000" class="form-control {{ $errors->has('data_nascimento') ? ' is-invalid' : '' }}" value="{{ old('data_nascimento') }}">
                                            @if ($errors->has('data_nascimento'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('data_nascimento') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group offset-1 col-md-6 col-lg-6">
                                <label>Contato</label>
                                <div class="form-group row has-success">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text">@</span></div>
                                            <input type="text" name="telefone" placeholder="(99)9 9999-9999 " class="form-control {{ $errors->has('telefone') ? ' is-invalid' : '' }}" value="{{ old('telefone') }}">
                                            @if ($errors->has('telefone'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('telefone') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>E-mail</label>
                            <div class="form-group row has-success">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <div class="input-group-prepend"><span class="input-group-text">@</span></div>
                                        <input type="email" name="email" placeholder="example@example.com" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}">
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <select-createdemanda></select-createdemanda>

                        <div class="form-group">
                            <label>Descrição do problema</label>
                            <div class="form-group row has-success">
                                <div class="col-sm-12">
                                    <div class="input-group">
                                        <div class="input-group-prepend"><span class="input-group-text">@</span></div>
                                        <textarea rows="5" name="descricao" placeholder="Escreva com todos os detalhes necessarios a situação ou problema encontrado!" class="form-control {{ $errors->has('descricao') ? ' is-invalid' : '' }}" >{{ old('descricao') }}</textarea>
                                        @if ($errors->has('descricao'))
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('descricao') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group float-left">
                            <a href="{{route('welcome')}}" class="btn btn-warning"><i class="fa fa-close"></i> Voltar</a>
                        </div>
                        <div class="form-group float-right">
                           <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Demandar</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>
@endsection
<script>
    import SelectCreatedemanda
    export default {
        components: {SelectCreatedemanda}
    }
</script>