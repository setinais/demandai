
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


Vue.component('example-component', require('./components/ToogleMenuComponent.vue'));

    //Demandai
    Vue.component('home-component', require('./components/demandai/HomeComponent.vue'));
    Vue.component('select-createdemanda', require('./components/demandai/SelectCreatedemanda.vue'));

    //User
    Vue.component('chart-line-home', require('./components/user/ChartLineHomeComponent.vue'));
    Vue.component('chart-rosca-home', require('./components/user/ChartRoscaHomeComponent.vue'));

    //Laboratorio
    Vue.component('projects', require('./components/laboratorio/datatable/Projects.vue'));
    Vue.component('projects-client-side', require('./components/laboratorio/datatable/ProjectsClientSide.vue'));

const app = new Vue({
    el: '#app'
});
