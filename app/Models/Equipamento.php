<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Equipamento extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function institution(){
        return $this->belongsTo('App\Models\Institution');
    }

    public function laboratorio(){
        return $this->belongsTo('App\Models\Laboratorio');
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
