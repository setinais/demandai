<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Laboratorio extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function institution()
    {
        return $this->belongsTo('App\Models\Institution');
    }

    public function equipamentos()
    {
        return $this->hasMany('App\Models\Equipamento');
    }

    public function scopeOrder($query)
    {
        return $query->orderBy('created_at', 'DESC');
    }
}
