<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Demand extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'nome', 'cpf', 'data_nascimento','telefone','email','action','action_id', 'descricao', 'codigo'
    ];
}
