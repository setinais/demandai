<?php

namespace App\Models;

use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use phpDocumentor\Reflection\Types\Boolean;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;


/**
 * Class User.
 *
 * @package namespace App\Entities;
 */
class User extends Authenticatable implements Transformable
{
    use TransformableTrait, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','formation','lattes','status','institution_id'
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new \App\Notifications\ResetPassword($token));
    }

    public function accessControl(){
        return $this->hasMany('App\Models\AccessControl');
    }
    public function laboratorios(){
        return $this->hasMany('App\Models\Laboratorio');
    }
    public function equipamentos(){
        return $this->hasMany('App\Models\Equipamento');
    }
    public function servicos(){
        return $this->hasMany('App\Models\Equipamento');
    }

    public function permissao($module, $action){
        $data = false;
        $access = $this->accessControl;
        foreach ($access as $control) {
            if($control->module->module == $module){
                if($control[$action]){
                    $data = true;
                }
            }
        }

        return $data;
    }

    public function menu(){
        $menu = [];

        $access = $this->accessControl;
        if(is_null($access) || empty($access)){
            $menu['vazio'] = 'Você ainda não tem permissôes no sistema!';
        }else{
            foreach ($access as $control) {
                $menu[strtolower($control->module->module)] = [
                    'create' => $control->create,
                    'update' => $control->update,
                    'delete' => $control->delete,
                    'select' => $control->select,
                    'module' => strtolower($control->module->module),
                    'description' => $control->module->description,
                    'icon' => $control->module->icon
                ];
            }
        }
        return $menu;
    }
}