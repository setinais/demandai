<?php

namespace App\Rules;

use App\Models\Demand;
use Illuminate\Contracts\Validation\Rule;

class DemandExist implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $demand = null;
        $demand = Demand::where($attribute, $value)->where('visualizada', false)->get();
        if(is_null($demand) || count($demand) === 0)
            return true;

        return false;

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'O :attribute já contém demanda em andamento!.';
    }
}
