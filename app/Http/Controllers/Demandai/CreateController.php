<?php

namespace App\Http\Controllers\Demandai;

use App\Mail\DemandaSolicitada;
use App\Models\Demand;
use App\Rules\CPF;
use App\Rules\DemandExist;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class CreateController extends Controller
{

    public function index(){
        return view('demandai.demandai');
    }

    public function indexSelecionado($action, $id, $nome){
        return view('demandai.demandai')->with(['action' => $action, 'id_action' => $id, 'nome' => $nome]);
    }

    public function store(Request $request){
        $validation = Validator::make(
            $request->all(),
            [
                'nome' => 'required|string',
                'cpf' => ['required','size:11', new DemandExist(), new CPF()],
                'data_nascimento' => 'required|date',
                'telefone' => 'required|integer|min:10',
                'email' => 'required|email',
                'action' => 'string',
                'action_id' => 'integer',
                'descricao' => 'required|min:20'
            ],
            [
                'required' => 'Campo Obrigatório!',
                'string' => 'Campo Inválido!',
                'size' => 'Campo deve conter :size caracteres!',
                'integer' => 'Campo não é numerico!',
                'email' => 'Campo Inválido!',
                'min' => 'Campo deve conter no minimo :size caracteres!',
                'exists' => 'O :atributte ja tem demanda pendente!'
            ]);

        if($validation->fails()){
            return redirect('demandai/create')->with(['action' => $request['action'], 'id_action' => $request['action_id'], 'nome' => $request['nome']])->withErrors($validation)->withInput();
        }

        $request['codigo'] = strtoupper(bin2hex(random_bytes(1)));
        $request['codigo'] .= strtoupper(bin2hex(random_bytes(2)));
        $request['codigo'] .= strtoupper(bin2hex(random_bytes(1)));
        $request['codigo'] .= strtoupper(substr(bin2hex(random_bytes(1)), 1));
        $demanda = Demand::create($request->all());
        Mail::to($request['email'])->send(new DemandaSolicitada($demanda));

        return redirect('demandai/create')->with('demanda', 'Demanda enviada com sucesso. Seu codigo de consulta é o '.$request['codigo'].' e também encaminhamos por e-mail!');
    }


}
