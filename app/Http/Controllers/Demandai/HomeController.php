<?php

namespace App\Http\Controllers\Demandai;

use App\Models\Institution;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Laboratorio;
use App\Models\Equipamento;
use App\Models\Servico;

class HomeController extends Controller
{

    public function getLaboratoriosEquipamentosServicos(){

        $ultimo_lab = Laboratorio::orderBy('id', 'desc')->first();
        $ultimo_equi = Equipamento::orderBy('id', 'desc')->first();
        $ultimo_ser = Servico::orderBy('id', 'desc')->first();

        $dados_lab = [];
        $dados_equi = [];
        $dados_ser = [];

        if($ultimo_equi->id <= 7){
            $dados_equi = Equipamento::all();
        }else{
            for($i = 0; $i<7;$i++){
                $dados_equi[] = Equipamento::find(rand(1,$ultimo_equi->id));
            }

        }
        if($ultimo_lab->id <= 7){
            $dados_lab = Laboratorio::all();
        }else{
            for($i = 0; $i<7;$i++){
                $dados_lab[] = Laboratorio::find(rand(1,$ultimo_lab->id));
            }
        }
        if($ultimo_ser->id <= 7){
            $dados_ser = Servico::all();
        }else{
            for($i = 0; $i<7;$i++){
                $dados_ser[] = Servico::find(rand(1,$ultimo_ser->id));
            }
        }
        $dados = [];
        for($i=0;$i<7;$i++){
            $dados[$i] = [
                'laboratorio' => ( array_key_exists($i,$dados_lab) ? $dados_lab[$i]->nome : ''),
                'lab_id' => ( array_key_exists($i,$dados_lab) ? $dados_lab[$i]->id : ''),

                'ser_id' => ( array_key_exists($i,$dados_ser) ? $dados_ser[$i]->id : ''),
                'servico' => ( array_key_exists($i,$dados_ser) ? $dados_ser[$i]->nome : ''),

                'equi_id' => ( array_key_exists($i,$dados_equi) ? $dados_equi[$i]->id : ''),
                'equipamento' => ( array_key_exists($i,$dados_equi) ? $dados_equi[$i]->nome : ''),

            ];
        }
        return $dados;
    }

    public function getDetailsLab($id){
        $lab = Laboratorio::find($id);
        $servidores = User::find(json_decode($lab->servidores));
        $servidores_min = null;
        foreach ($servidores as $ser){
            $servidores_min[] = $ser->name;
        }
        $equipamentos = null;
        foreach ($lab->equipamentos as $equi){
            $equipamentos[] = [
                'descricao' => $equi->descricao,
                'nome' => $equi->nome,
            ];
        }

        $dados = [
            'id' => $lab->id,
            'coordenador' => [
                'nome' => $lab->user->name,
                'email' => $lab->user->email,
            ],
            'descricao' => $lab->descricao,
            'nome' => $lab->nome,
            'telefone' => $lab->telefone,
            'atividades_realizadas' => $lab->atividades_realizadas,
            'pesquisa_extensao' => $lab->pesquisa_extensao,
            'instituicao' => $lab->institution->name,
            'endereco_sala' => $lab->endereco_sala,
            'servidores' => $servidores_min,
            'departamentos' => empty(json_decode($lab->departamentos)) ? null : json_decode($lab->departamentos),
            'cursos' => empty(json_decode($lab->cursos)) ? null : json_decode($lab->cursos),
            'equipamentos' => $equipamentos
        ];

        return $dados;
    }
    public function getDetailsEqui($id){
        $equipamento = Equipamento::find($id);

        $dados = [
            'id' => $equipamento->id,
            'coordenador' => [
                'nome' => $equipamento->user->name,
                'email' => $equipamento->user->email,
            ],
            'descricao' => $equipamento->descricao,
            'nome' => $equipamento->nome,
            'laboratorio' => (is_null($equipamento->laboratorio) || empty($equipamento->laboratorio) ? null : ['nome' => $equipamento->laboratorio->nome, 'id' => $equipamento->laboratorio->id]),
            'instituicao' => $equipamento->institution->name,
        ];

        return $dados;
    }
    public function getDetailsSer($id){
        $servico = Servico::find($id);

        $dados = [
            'id' => $servico->id,
            'coordenador' => [
                'nome' => $servico->user->name,
                'email' => $servico->user->email,
            ],
            'descricao' => $servico->descricao,
            'nome' => $servico->nome,
            'plataformas' => $servico->plataformas,
            'instituicao' => $servico->institution->name,
            'departamentos' => empty(json_decode($servico->departamentos)) ? null : json_decode($servico->departamentos),
            'desenvolvedores' =>  empty(json_decode($servico->desenvolvedores)) ? null : json_decode($servico->departamentos),
        ];

        return $dados;
    }

    public function getLaboratorios(){
        $data = null;
        $lab = Laboratorio::all();
        foreach ($lab as $vals){
            $data[] = [
                'nome' => $vals->nome,
                'id' => $vals->id
            ];
        }
        return $data;

    }
    public function getServicos(){
        $data = null;
        $ser = Servico::all();
        foreach ($ser as $vals){
            $data[] = [
                'nome' => $vals->nome,
                'id' => $vals->id
            ];
        }
        return $data;
    }
    public function getEquipamentos(){
        $data = null;
        $equ = Equipamento::all();
        foreach ($equ as $vals){
            $data[] = [
                'nome' => $vals->nome,
                'id' => $vals->id
            ];
        }
        return $data;
    }
}
