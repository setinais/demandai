<?php

namespace App\Http\Controllers\Laboratorios;

use App\Models\Equipamento;
use App\Models\Laboratorio;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;

class HomeController extends Controller
{
    public function index(){
        return view('laboratorios.home');
    }

    public function details($id){
        $laboratorio = null;
        $lab =  Laboratorio::find($id);
        return view('laboratorios.details')->with('laboratorios', $laboratorio);
    }


}
