<?php

namespace App\Http\Controllers\Laboratorios;

use App\Models\Equipamento;
use App\Models\Laboratorio;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;

class DatatableController extends Controller
{
    public function index(Request $request)
    {
        if ( $request->input('client') ) {
            $labs = [];
            $laboratorios = Laboratorio::order()->get();
            foreach ($laboratorios as $val){
                $labs[] = [
                    'id' => $val->id,
                    'nome' => $val->nome,
                    'telefone' => $val->telefone,
                    'responsavel' => $val->user->name,
                    'pesext' => $val->pesquisa_extensao,
                    'equipamentos' => Equipamento::where('laboratorio_id', $val->id)->count(),
                    'status' => $val->status,
                    'created' => Carbon::parse($val->created_at)->format('d/m/Y'),
                    'modified' => Carbon::parse($val->updated_at)->format('d/m/Y'),
                ];
            }
            return $labs;
        }

        $columns = ['updated_at', 'created_at', 'nome'];

        $length = $request->input('length');
        $column = $request->input('column'); //Index
        $dir = $request->input('dir');
        $searchValue = $request->input('search');

        $query = Laboratorio::select('id', 'telefone', 'descricao', 'nome', 'status')->orderBy($columns[$column], $dir);

        if ($searchValue) {
            $query->where(function($query) use ($searchValue) {
                $query->where('nome', 'like', '%' . $searchValue . '%')
                    ->orWhere('descricao', 'like', '%' . $searchValue . '%');
            });
        }

        $projects = $query->paginate($length);
        return ['data' => $projects, 'draw' => $request->input('draw')];
    }
}
