<?php

namespace App\Http\Controllers;

use App\Models\Institution;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	return view('user.home');
    }

    public function getPerfil($status){
        switch($status){
            case 0:
                return view('user.perfil')->with('instituicoes', Institution::all());
                break;
            case 1:
                return view('user.perfil')->with(['instituicoes' => Institution::all(), 'msg' => ['msg' => 'Alguns campos encontram invalidos!', 'type' => 'danger']]);
                break;
            case 2:
                return view('user.perfil')->with(['instituicoes' => Institution::all(), 'msg' => ['msg' => 'Perfil Atualizado com sucesso!', 'type' => 'success']]);
                break;
        }
    }

    public function updatePerfil(Request $request){
        $campos_validar = [
            'name' => 'required|string|max:255',
            'formation' => 'required',
            'lattes' => 'required',
            'institution' => 'required',
        ];
        if(!empty($request['password']))
            $campos_validar['password'] = "string|min:6|confirmed";

        $validation = Validator::make($request->all(), $campos_validar);
        if($validation->fails()){
            return redirect()->route('perfil', ['status' => 1])->withErrors($validation);
        }
        $user = User::find(Auth::user()->id);
        $user->name = $request['name'];
        $user->formation = $request['formation'];
        $user->lattes = $request['lattes'];
        $user->institution_id = $request['institution'];
        $user->password = Hash::make($request['password']);
        $user->save();

        return redirect()->route('perfil', ['status' => 2]);
    }
}
