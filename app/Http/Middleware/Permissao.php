<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Permissao
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $module, $action)
    {
        if(!Auth::user()->permissao($module,$action)){
            return redirect('/home')->with('deletarErro', 'Usuario não Autorizado!');
        }
        return $next($request);
    }
}
