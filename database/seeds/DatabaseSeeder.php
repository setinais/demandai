<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call( PopulacaoSeeder::class);
        //$this->call( LabEquiTableSeeder::class);
        factory(App\Models\Laboratorio::class, 20)->create();
        factory(App\Models\Equipamento::class, 100)->create();
        factory(App\Models\Servico::class, 20)->create();
    }
}
