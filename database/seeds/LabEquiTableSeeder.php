<?php

use Illuminate\Database\Seeder;

class LabEquiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Laboratorio
        factory(App\Models\Laboratorio::class, 20)->create();
        factory(App\Models\Equipamento::class, 100)->create();
        factory(App\Models\Servico::class, 20)->create();
    }
}
