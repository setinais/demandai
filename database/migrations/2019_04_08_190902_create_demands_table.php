<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDemandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('demands', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('action',['ser','lab','equ']);
            $table->integer('action_id');
            $table->string('nome');
            $table->string('cpf');
            $table->date('data_nascimento');
            $table->string('telefone');
            $table->string('email');
            $table->string('codigo');
            $table->longText('descricao');
            $table->boolean('visualizada')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('demands');
    }
}
