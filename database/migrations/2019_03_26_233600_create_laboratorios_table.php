<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaboratoriosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laboratorios', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id');
            $table->string('telefone');
            $table->longText('descricao');
            $table->string('nome');
            $table->string('atividades_realizadas');
            $table->boolean('pesquisa_extensao');
            $table->bigInteger('institution_id');
            $table->string('endereco_sala');
            $table->json('servidores')->nullable();
            $table->json('departamentos')->nullable();
            $table->json('cursos')->nullable();
            $table->boolean('status')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laboratorios');
    }
}
