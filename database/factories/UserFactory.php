<?php
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
$factory->define(App\Models\Laboratorio::class, function (Faker $faker) {

    $number = $faker->numberBetween(1,10);
    $cur = null;
    $dep = null;
    for ($i=0;$i<$number;$i++){
        $cur[] = $faker->name;
        $dep[] = $faker->name;
    }

    return [
        'user_id'=> 1,
        'nome' => $faker->name,
        'descricao' => $faker->sentence,
        'endereco_sala' => $faker->address,
        'telefone' => $faker->phoneNumber,
		'atividades_realizadas'=> $faker->sentence,
		'pesquisa_extensao'=> $faker->boolean,
		'institution_id'=> $faker->numberBetween(1,12),
		'servidores'=> json_encode([1,2]),
        'cursos'=> json_encode($cur),
        'departamentos'=> json_encode($dep),
        'status' => $faker->boolean,
    ];
});

$factory->define(App\Models\Equipamento::class, function (Faker $faker) {
    return [
        'user_id'=> 1,
        'nome' => $faker->name,
        'descricao' => $faker->sentence,
        'codigo' => Str::random(10),
        'institution_id'=> $faker->numberBetween(1,12),
        'laboratorio_id'=> $faker->numberBetween(1,20),
        'status' => $faker->boolean,
    ];
});

$factory->define(App\Models\Servico::class, function (Faker $faker) {
    $number = $faker->numberBetween(1,10);
    $des = null;
    $dep = null;
    for ($i=0;$i<$number;$i++){
        $des[] = $faker->name;
        $dep[] = $faker->name;
    }

    return [
        'user_id'=> 1,
        'nome' => $faker->name,
        'descricao' => $faker->sentence,
        'plataformas' => $faker->chrome,
        'institution_id'=> $faker->numberBetween(1,12),
        'servidores'=> json_encode([1,2]),
        'desenvolvedores'=> json_encode($des),
        'departamentos'=> json_encode($dep),
        'status' => $faker->boolean,
    ];
});