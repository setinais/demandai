<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/inicio',function(){
    return view('templates_saymon.home');
});

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Auth::routes([]);

Route::get('/testes/',function (){
    $invoice = App\Models\Demand::find(1);

    return new App\Mail\DemandaSolicitada($invoice);
});
//Demandai

Route::namespace('Demandai')->group(function (){

    Route::get('demandai','HomeController@index')->name('demandai');

    Route::prefix('demandai')->group(function () {
        Route::get('create', 'CreateController@index')->name('demandai.formulario');
        Route::get('create/{action}/{id_action}/{nome}', 'CreateController@indexSelecionado')->name('demandai.formularioSelecionado');
        Route::post('create', 'CreateController@store')->name('demandai.store');
    });
});


Route::middleware(['auth'])->group(function (){

    Route::get('/home/', 'HomeController@index')->name('home');
    Route::get('/perfil/{status}/', 'HomeController@getPerfil')->name('perfil');
    Route::post('/perfilupdate/', 'HomeController@updatePerfil')->name('updateperfil');

    //Laboratorio
    Route::namespace('Laboratorios')->group(function (){

        Route::get('laboratorios','HomeController@index')->middleware('permissao:Laboratorios,select')->name('laboratorios');

        Route::prefix('laboratorios')->group(function () {

            Route::get('details/{id}', 'HomeController@details')->name('laboratorio.details');
        });
    });
    //Equipamentos

//    Route::namespace('Equipamento')->group(function (){
//
//        Route::get('equipamento','HomeController@index')->middleware('permissao:Equipamento,select')->name('equipamento');
//
//        Route::prefix('equipamento')->group(function () {
//            //Route::get('create','HomeController@s');
//        });
//    });

    //Serviços

    Route::namespace('Servicos')->group(function (){

        Route::get('servicos','HomeController@index')->middleware('permissao:Servicos,select')->name('servicos');

        Route::prefix('servicos')->group(function () {
            //Route::get('create','HomeController@s');
        });
    });

    //Administrador

    Route::namespace('Administrador')->group(function (){

        Route::get('administrador','HomeController@index')->middleware('permissao:Administrador,select')->name('administrador');

        Route::prefix('administrador')->group(function () {
            //Route::get('create','HomeController@s');
        });
    });
});
